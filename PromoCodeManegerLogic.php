<?php
/*
Plugin Name: Promo Code Manager
Plugin URI: https://bitbucket.org/direx/wp-promo-code-manager
Description: Управление промокодами
Version: 1.0
Author: Nikolay Ponich
Author URI: http://ponich.com
*/

// драйвер базы данных
global $wpdb;

// Библиотека для управления плагином
include_once 'src/Promo.php';

// Менеджер
include_once 'src/Manager.php';

// Виды для админки
include_once 'src/View.php';

// AJAX плагина
include_once 'src/Ajax.php';

// Экземпляр плагина
$PromoCodeManeger = new Ponich\PromoManager\Promo($wpdb, plugin_dir_path(__FILE__));

// Хук на инстализацию/активацию плагина
register_activation_hook(__FILE__, [
    $PromoCodeManeger,
    'install'
]);

// Хук объявление страниц плагина в админке
add_action('admin_menu', [
    $PromoCodeManeger->view(),
    'init'
]);

// Тестовый метод: wp-admin/admin-ajax.php?action=promocodemanager_test
$PromoCodeManeger->ajax()
    ->route('test', 'testMethod');

// Вывод списка промокодов: wp-admin/admin-ajax.php?action=promocodemanager_admin-all-codes
$PromoCodeManeger->ajax()
    ->route('admin-all-codes', 'getAllCodes');

// Добавления новых кодов: wp-admin/admin-ajax.php?action=promocodemanager_admin-create
$PromoCodeManeger->ajax()
    ->route('admin-create', 'create');
