<?php

namespace Ponich\PromoManager;

/**
 * Class Promo
 * Библиотека отвечающая за инстализацию модуля в WP
 * Работает как фасад, предоставляя доступ к другим обьектам плагина
 *
 * @package Ponich\PromoManager
 */
class Promo
{
    /**
     * Драйвер для работы с базой
     * @var \wpdb
     */
    protected $wpdb;

    /**
     * Путь к каталогу плагина, с слешем в конце
     * @var string
     */
    protected $plugin_dir;

    /**
     * @var
     */
    protected $jal_db_version = '1.0';

    /**
     * @var array
     */
    protected $dumpFindIn = [];

    /**
     * @var array
     */
    protected $dumpFindOut = [];

    /**
     * @var Manager
     */
    protected $Manager;

    /**
     * @var View
     */
    protected $View;

    /**
     * @var Ajax
     */
    protected $Ajax;

    /**
     * Promo constructor.
     * @param $wpdb \wpdb
     * @param string $pluginDir
     */
    public function __construct($wpdb, $pluginDir = '../')
    {
        $this->wpdb = $wpdb;
        $this->plugin_dir = $pluginDir;

        // настройки замены значений в дамп файлах
        // входящие данные (маска поиска)
        $this->dumpFindIn = [
            '%prefix%',
            '%charset%'
        ];

        // исходящие данные
        $this->dumpFindOut = [
            $this->wpdb->prefix, // прнфикс таблици
            $this->wpdb->get_charset_collate() // кодировка
        ];

        //
        $this->Manager = new Manager($this->wpdb);
        $this->View = new View("{$this->plugin_dir}views/");
        $this->Ajax = new Ajax($this->wpdb, $this->manager());
    }

    /**
     * Менеджер для работы с промо-кодами
     * @return Manager
     */
    public function manager()
    {
        return $this->Manager;
    }

    /**
     * @return View
     */
    public function view()
    {
        return $this->View;
    }

    /**
     * @return Ajax
     */
    public function ajax()
    {
        return $this->Ajax;
    }

    /**
     * Установка плагина
     */
    public function install()
    {
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        $installed_ver = get_option("jal_db_version");

        if ($installed_ver != $this->jal_db_version) {
            // таблица promo_manager_codes
            $sql = $this->getDump('promo_manager_codes.sql');
            dbDelta($sql);
            update_option("jal_db_version", $this->jal_db_version);

            // таблица promo_manager_code_uses
            $sql = $this->getDump('promo_manager_code_uses.sql');
            dbDelta($sql);
            update_option("jal_db_version", $this->jal_db_version);
        }
    }

    /**
     * Вернет файл дампа
     * @param string $fileName имя дамп файла
     * @return bool|string вернет SQL дамп таблици, если файл найден. В ином случаи вернет FALSE
     */
    protected function getDump($fileName)
    {
        $filePathName = "{$this->plugin_dir}/sql/{$fileName}";

        if (!file_exists($filePathName)) {
            return false;
        }

        return str_replace(
            $this->dumpFindIn,
            $this->dumpFindOut,
            file_get_contents($filePathName)
        );

    }

}