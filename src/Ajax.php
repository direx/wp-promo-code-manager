<?php

namespace Ponich\PromoManager;

/**
 * Class Ajax
 * Класс для управления AJAX запросами плагина
 * Запросы как админки так и пользовательской части
 *
 * @package Ponich\PromoManager
 */
class Ajax
{
    /**
     * Драйвер для работы с базой
     * @var \wpdb
     */
    protected $wpdb;

    /**
     * @var string
     */
    protected $prefix = 'promocodemanager_';

    /**
     * @var Manager;
     */
    protected $Manager;

    /**
     * Ajax constructor.
     * @param $wpdb
     */
    public function __construct($wpdb, $manager)
    {
        $this->wpdb = $wpdb;
        $this->Manager = $manager;
    }

    /**
     * Тестовый запрос
     */
    public function testMethod()
    {
        wp_send_json([
            'action' => 'test',
            'status' => 200,
            'response' => [
                'echo' => 'hello world'
            ],
        ]);
    }

    /**
     * Вернет все промокоды
     */
    public function getAllCodes()
    {
        $this->onlyAdmin();

        wp_send_json([
            'status' => 200,
            'response' => $this->Manager->get(),
        ]);
    }

    /**
     * Создания новых промокодов
     */
    public function create()
    {

        $post = [
            'promo_code' => (isset($_POST['promo_code'])) ? $_POST['promo_code'] : null,
            'is_reusable' => (isset($_POST['is_reusable'])) ? $_POST['is_reusable'] : null,
            'expiration_at' => (isset($_POST['expiration_at'])) ? $_POST['expiration_at'] : null,
        ];


        $count = 0;

        foreach (explode("\n", $post['promo_code']) as $code) {
            $code = trim($code);
            $code = str_replace(["\n", "\r"], ["", ""], $code);

            if (mb_strlen($code) < 2) {
                continue;
            }

            $expDate = (new \DateTime($post['expiration_at']))->getTimestamp();

            $data = [
                'promo_code' => $code,
                'is_reusable' => ($post['is_reusable']) ? 1 : 0,
                'create_at' => date("Y-m-d H:i:s"),
                'expiration_at' => date("Y-m-d H:i:s", $expDate),
            ];


            if ($this->Manager->add($data)) {
                $count++;
            }
        }


        wp_send_json([
            'status' => 200,
            'response' => $count,
        ]);
    }

    /**
     * Добавить роут обработчик для AJAX
     *
     * @param $route
     * @param string $method
     * @throws \ErrorException
     */
    public function route($route, $method = 'testMethod')
    {
        if (!method_exists($this, $method)) {
            throw new \ErrorException('Метод не найден', 1002);
        }

        add_action("wp_ajax_{$this->prefix}{$route}", [
            $this,
            $method
        ]);
    }

    /**
     * Валидация на авторизацию админа
     * Запрос будет доступен только администратору
     */
    protected function onlyAdmin()
    {
        if (!is_admin()) {
            wp_send_json([
                'status' => 401,
                'error' => 'Unauthorized',
            ]);
        }
    }

}
