<?php

namespace Ponich\PromoManager;

/**
 * Class View
 * Класс для управления видами плагина
 *
 * @package Ponich\PromoManager
 */
class View
{
    /**
     * @var string
     */
    protected $view_dir;

    /**
     * @var array
     */
    protected $defaultTemplateIn = [];

    /**
     * @var array
     */
    protected $defaultTemplateOut = [];

    /**
     * View constructor.
     * @param string $viewDir
     */
    public function __construct($viewDir = '../')
    {
        $this->view_dir = $viewDir;
    }

    /**
     * Инстализация странц плагина в админке
     */
    public function init()
    {
        // главная
        add_menu_page('Менеджер промокодов', 'Промокоды', 'read', 'promocodemanager', [
            $this,
            'pageIndex'
        ], 'dashicons-tickets-alt');

        // добавить код
        add_submenu_page('promocodemanager', 'Создания нового промокода', 'Создать', 'read', 'promocodemanager-create', [
            $this,
            'pageCreate'
        ]);
    }

    /**
     * Главная страница плагина "Промокоды"
     */
    public function pageIndex()
    {
        $this->echoView('index.php');
    }

    /**
     * Страница просмотра логов промокода "История"
     */
    public function pageHistory()
    {
        $this->echoView('history.php');
    }

    /**
     * Страница добавления нового промокода "Создать"
     */
    public function pageCreate()
    {
        $this->echoView('create.php');
    }

    /**
     * Страница настроек плагина "Настройки"
     */
    public function pageSetting()
    {
        $this->echoView('setting.php');
    }

    /**
     * Вернет шаблон
     * @param $view
     * @param array $i
     * @param array $o
     * @return mixed
     * @throws \ErrorException
     */
    protected function getView($view, $i = [], $o = [])
    {
        $filePathName = "{$this->view_dir}$view";

        if (!file_exists($filePathName)) {
            throw new \ErrorException('Файл шаблона не найден', 1001);
        }

        // буферизация вывода
        ob_start();
        include_once $filePathName;
        $content = ob_get_contents();
        ob_end_clean();

        return str_replace(array_merge($i, $this->defaultTemplateIn), array_merge($o, $this->defaultTemplateOut), $content);
    }

    /**
     * Выведет шаблон
     * @param $view
     * @param array $i
     * @param array $o
     */
    protected function echoView($view, $i = [], $o = [])
    {
        echo $this->getView($view, $i, $o);
    }

}