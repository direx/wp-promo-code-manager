<?php

namespace Ponich\PromoManager;

/**
 * Class Manager
 * Библиотека для работы с промо-кодами
 * Отвечает за использование, проверку и добавление кодов в базу
 *
 * @package Ponich\PromoManager
 */
class Manager
{
    /**
     * Драйвер для работы с базой
     * @var \wpdb
     */
    protected $wpdb;

    /**
     * Manager constructor.
     * @param $wpdb
     */
    public function __construct($wpdb)
    {
        $this->wpdb = $wpdb;
    }

    /**
     * Проверка валидности кода
     * @param null|string $code
     * @return bool
     */
    public function isValiable($code = null)
    {
        if (is_null($code)) {
            return false;
        }

        // посмотрим на сам код
        $sql = "SELECT * FROM {$this->wpdb->prefix}promo_manager_codes WHERE promo_code = '{$code}' LIMIT 1";
        $code = $this->wpdb->get_results($sql);

        // если код не найден, скажем что он не валидный
        if (!$code) {
            return false;
        }

        $code = $code[0];
        $codeExp = strtotime($code->expiration_at);

        // если код просроченный,Ю скажем что он не валидный
        if (time() > $codeExp) {
            return false;
        }

        $sql = "SELECT * FROM {$this->wpdb->prefix}promo_manager_code_uses WHERE promo_code_id = '{$code->id}'";
        $use = $this->wpdb->get_results($sql);

        // если код не использовался, скажем что он валидный
        if (!$use) {
            return true;
        } // если код использовался, проверим или он многоразовый
        else {
            // код одноразовый
            if (!$code->is_reusable) {

                $isUse = false;

                foreach ($use as $u) {
                    if ($u->action == 'activate') {
                        $isUse = true;
                    }
                }

                // если негде не юзался, то валидный
                return ($isUse) ? false : true;
            } else {
                // код многоразовый
                return true;
            }
        }
    }

    public function usePromo($promo = null)
    {
        $this->call($promo);

        $sql = "SELECT * FROM {$this->wpdb->prefix}promo_manager_codes WHERE promo_code = '{$promo}'";
        $code = $this->wpdb->get_results($sql);

        $id = (isset($code[0]->id)) ? $code[0]->id : null;

        $sql = "SELECT 
                    count(*) AS count
                FROM {$this->wpdb->prefix}promo_manager_code_uses
                WHERE promo_code_id = {$id}";

        $howUse = $this->wpdb->get_results($sql);

        $countCall = (count($howUse)) ? (isset($howUse[0]->count)) ? $howUse[0]->count : 0 : 0;

        return [
            'is_valiable' => $this->isValiable($promo),
            'count_call' => $countCall,
        ];
    }

    /**
     * Проверит существует ли код
     * Если код найден, вернет его ID
     * @param null|string $code
     * @return bool|integer
     */
    public function isExist($code = null)
    {
        if (is_null($code)) {
            return false;
        }

        // посмотрим на сам код
        $sql = "SELECT * FROM {$this->wpdb->prefix}promo_manager_codes WHERE promo_code = '{$code}' LIMIT 1";
        $code = $this->wpdb->get_results($sql);

        return ($code) ? $code[0]->id : false;
    }

    /**
     * Создать новый промо-код
     *
     * @param $data
     * @return false|int
     */
    public function add($data)
    {
        if (!$this->isExist($data['promo_code'])) {
            return $this->wpdb->insert("{$this->wpdb->prefix}promo_manager_codes", $data);
        }

        return false;
    }


    /**
     * Вернет все коды
     * @param int $offset
     * @param int $limit
     * @return array|null|object
     */
    public function get($offset = 0, $limit = 9999)
    {
        $sql = "SELECT
                  c.*,
                  (SELECT COUNT(*)
                   FROM {$this->wpdb->prefix}promo_manager_code_uses u
                   WHERE u.promo_code_id = c.id AND u.action = 'call')     AS 'count_call',
                  (SELECT COUNT(*)
                   FROM {$this->wpdb->prefix}promo_manager_code_uses a
                   WHERE a.promo_code_id = c.id AND a.action = 'activate') AS 'count_activate'
                
                FROM {$this->wpdb->prefix}promo_manager_codes c
                
                ORDER BY count_call DESC, count_activate DESC
                
                LIMIT {$limit} OFFSET {$offset}";

        return $this->wpdb->get_results($sql);
    }

    /**
     * Активация кода
     * @param null|string $code
     * @return bool
     */
    public function active($code = null, $action = 'activate')
    {
        if ($this->isValiable($code) && $id = $this->isExist($code)) {
            // активируем код
            return (bool)$this->wpdb->insert("{$this->wpdb->prefix}promo_manager_code_uses", [
                'promo_code_id' => $id,
                'action' => 'activate',
                'update_at' => date('Y-m-d H:i:s'),
            ]);
        }

        return false;
    }

    /**
     * Вызов кода
     * @param null $code
     * @return bool|false|int
     */
    public function call($code = null)
    {
        if (!$id = $this->isExist($code)) {
            return false;
        }

        return (bool)$this->wpdb->insert("{$this->wpdb->prefix}promo_manager_code_uses", [
            'promo_code_id' => $id,
            'action' => 'call',
            'update_at' => date('Y-m-d H:i:s'),
        ]);
    }

}