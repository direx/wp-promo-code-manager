var PromoCode = new Object({
    baseUrl: null,

    init: function () {
        console.log('Promo Code Manager');

        this.baseUrl = ajaxurl;
    },

    /**
     * Загрузить промокоды
     */
    promocodemanager: function () {
        var $self = this;

        jQuery.get($self.baseUrl + '?action=promocodemanager_admin-all-codes', function (response) {
            if (response.status == 200) {

                var html = '';
                jQuery.each(response.response, function (i, item) {
                    var is_reusable = (item.is_reusable == 1) ? 'Да' : 'Нет';

                    html = html
                        + '<tr class="row">'
                        + '<th scope="row" class="check-column">'
                        + (i + 1)
                        + '</th>'
                        + '<th>' + item.promo_code + '</th>'
                        + '<th>' + is_reusable + '</th>'
                        + '<th>' + item.count_call + '</th>'
                        + '<th>' + item.count_activate + '</th>'
                        + '<th>' + item.expiration_at + '</th>'
                        + '<th>' + item.create_at + '</th>'
                        + '</tr>';

                });
                jQuery('#promo-index').html(html);
            } else {
                alert('Ошибка данных');
            }

            jQuery('.promo-loaded').hide();
        })
    },

    /**
     * Создать новые промокоды
     */
    promocodemanagerCreate: function () {
        var $self = this;

        jQuery('#promo-create').submit(function (e) {
            e.preventDefault();
            jQuery('.promo-loaded').show();

            var data = jQuery(this).serialize();

            jQuery.post($self.baseUrl + '?action=promocodemanager_admin-create', data, function (response) {
                if (response.status == 200) {
                    document.location.href = 'admin.php?page=promocodemanager';
                } else {
                    alert('Ошибка данных');
                }
                jQuery('.promo-loaded').hide();
            });
        });
    },

});

PromoCode.init();