<style>
    <?php
        $this->echoView('css/style.css');
    ?>
</style>

<div class="promo-loaded">
    <div class="promo-loaded-text">
        Загрузка <span>...</span>
    </div>
</div>


<div class="wrap">
    <h2>Промокоды</h2>

    <table class="widefat promo-table-index">
        <thead>
        <tr>
            <td><strong>#</strong></td>
            <th><strong>Промокод</strong></th>
            <th><strong>Многоразовый</strong></th>
            <th><strong>Обращений</strong></th>
            <th><strong>Активаций</strong></th>
            <th><strong>Срок годности</strong></th>
            <th><strong>Создан</strong></th>
        </tr>
        </thead>

        <tbody id="promo-index"></tbody>
    </table>
</div>


<script>
    <?php
    $this->echoView('js/common.js');
    ?>

    PromoCode.promocodemanager();
</script>