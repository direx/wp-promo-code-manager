<style>
    <?php
        $this->echoView('css/style.css');
    ?>
</style>


<div class="promo-loaded">
    <div class="promo-loaded-text">
        Загрузка <span>...</span>
    </div>
</div>

<div class="wrap">
    <h2>Добавить промокоды</h2>

    <form action="#" method="post" id="promo-create">

        <table class="form-table">
            <tr valign="top">
                <th scope="row">Срок годности</th>
                <td>
                    <input type="date" name="expiration_at" placeholder="ГГГГ-ММ-ДД" >
                </td>
            </tr>

            <tr valign="top">
                <th scope="row">Многоразовые</th>
                <td>
                    <select name="is_reusable">
                        <option value="0">Нет</option>
                        <option value="1">Да</option>
                    </select>
                </td>
            </tr>

            <tr valign="top">
                <th scope="row">
                    Промокод <br>
                    <span class="no-strong">
                        <i>можно несколько, каждый новый код на новой строке</i>
                    </span>
                </th>
                <td>
                    <textarea name="promo_code" cols="30" rows="10"></textarea>
                </td>
            </tr>

            <tr valign="top">
                <th scope="row"></th>
                <td>
                    <input type="submit" value="Создать">
                </td>
            </tr>
        </table>

    </form>


</div>


<script>
    <?php
    $this->echoView('js/common.js');
    ?>

    jQuery('.promo-loaded').hide();
    PromoCode.promocodemanagerCreate();
</script>