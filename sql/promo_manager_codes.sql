CREATE TABLE %prefix%promo_manager_codes
(
    id INT PRIMARY KEY AUTO_INCREMENT,
    promo_code VARCHAR(255),
    is_reusable BOOLEAN DEFAULT FALSE,
    create_at DATETIME DEFAULT '0000-00-00 00:00:00' NOT NULL,
    expiration_at DATETIME DEFAULT '0000-00-00 00:00:00' NOT NULL

)
%charset%;