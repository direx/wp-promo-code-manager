CREATE TABLE %prefix%promo_manager_code_uses
(
    id INT PRIMARY KEY AUTO_INCREMENT,
    promo_code_id INT,
    action VARCHAR(255),
    update_at DATETIME DEFAULT '0000-00-00 00:00:00' NOT NULL
) %charset%;